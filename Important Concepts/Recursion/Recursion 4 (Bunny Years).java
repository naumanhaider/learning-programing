import java.util.*;
class recur{
	public static void main (String str[]){
		Scanner q = new Scanner(System.in);
		int a,b,c = q.nextInt();
		b = bunny ( c );
		System.out.println(b);
	}
	public static int bunny ( int a ){
		if( a == 0 ) return 0;
		if( a == 1 ) return 2;
		if ( a%2 == 0 ) return 3 + bunny( a - 1 );
		return 2 + bunny ( a - 1);
	}
}