import java.util.*;
class Fibonacci 
{
	public static void main (String argument[])
	{
		Scanner scanner = new Scanner (System.in);
		int userInput;
		System.out.println("How many terms do you want to see of the Fibonacci numbers?");
		userInput = scanner.nextInt();
		fibonacci(0, 1, userInput - 2 );
		
	}
	public static void fibonacci (int a,int b,int userInput)
	{
		if (userInput == 0)
			System.exit(0);
		if(a == 0)
			System.out.print(a + " " + b + " ");
	
		int d = a + b;
		System.out.print(d+" ");
		userInput --;
		fibonacci(b,d,userInput);
		
	}
}