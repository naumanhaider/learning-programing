import java.util.*;
/*
* this program finds the factorial of the given number.
*/
class Recursion {
	public static void main(String argument[]) 
	throws java.io.IOException
	{
		int userInput;
		Scanner scanner = new Scanner(System.in);
		userInput = scanner.nextInt();
		int result = factorial(userInput);
		System.out.println("The factorial is: \n" + result);		
	}
	
	public static int factorial(int userInput)
	{
		if(userInput == 1) return 1;
		return userInput*factorial(userInput - 1);
	}
}