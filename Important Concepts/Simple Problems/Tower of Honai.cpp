#include<iostream>
#include<cstdlib>

using namespace std;

int moves = 0;

void towers (int disk, char from, char to, char spare);

int main ()
{
    int disk;
    char from,spare,to;
    cout<<"Please Enter the No. of Discs"<<endl;
    cin>>disk;
    cout<<"Please Enter the Letter of the stack the disks are in"<<endl;
    cin>>from;
    cout<<"Please Enter the Letter of the stack you want the disks to go to"<<endl;
    cin>>to;
    cout<<"Please Enter the Letter of the spare stack"<<endl;
    cin>>spare;
    towers(disk,from,to,spare);
    cout<<"The Minimum Number Of Moves are: "<<moves<<endl;
    system("pause");
    return 0;
}

void towers (int disk, char from, char to, char spare)
{
    if(disk == 1)
    {
        cout<<"Move Disk from "<<from<<" to "<<to<<endl;
        moves++;
    }
    else
    {
        towers(disk - 1, from , spare , to);
        towers(1 , from, to, spare);
        towers(disk - 1, spare , to , from);
    }
}
