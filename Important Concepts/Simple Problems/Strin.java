import java.util.Scanner;
import java.lang.*;
class ab 
{
	public static int stringLengthMain = -1;
	public static void main (String arg[])
	throws java.io.IOException
	{
		char usrInput[] = new char[30];
		char newStr[] = new char[30];
		do {
			stringLengthMain++;
			usrInput [stringLengthMain] = (char) System.in.read();
		} while (usrInput[stringLengthMain] != 13); // here the program takes a single charecter at a time as an input to the char array.
		newStr = rmvFirstParen(usrInput, stringLengthMain);
		int temp = check(newStr , stringLengthMain);
		if (temp == 1)
			System.out.println("Misalligned Brackets Detected");
		for( int strPoint = 0; strPoint < stringLengthMain; strPoint++) // it prints out the result that is sent back from those two functions.
		{
			System.out.print(newStr[strPoint]);
		}
	}
 
	public static char[] rmvFirstParen (char paren[], int stringLength) // no it finds and removes the first one and for the second one, it sends the string to another method.
	{
		if (stringLength == 0)
			return paren;
		if (paren[stringLength] == ')' )
		{
			int temp = check(paren , paren.length -1);
			if ( temp == 1)
			{
				stringLengthMain --;
				int pointer = stringLength - 1; //yeah so i gave the value of the stringlrngth ie. the point where the ')' is supposed to be.
				do
				{
					pointer++; // You are incrementing the search counter in a loop. 
					paren[pointer] = paren[pointer + 1]; // here the value which had the ')' is being replaced by the char after it and hence so forth.
				}while(paren[pointer] != '\0');// such that the string is now one charecter short.
				paren = rmvSecParen(paren, 0); // sends the string to another fuction that would remove its second bracket'(' if found.
			}
			else
			{
				System.out.println("Misalligned Brackets detected");
				return paren;
			}
		}
		return rmvFirstParen(paren, --stringLength);
	}
	
	public static char[] rmvSecParen (char paren[], int stringLength) //removes the second '(' bracket if found the error part i will do in a little bit of time.
	{
		if (paren[stringLength] == '\0')
			return paren;
		if (paren[stringLength] == '(' )
		{
			stringLengthMain --;
			int pointer= stringLength - 1;
			do
			{
				pointer++;
				paren[ pointer] = paren[pointer + 1];
			}while(paren[ pointer] != '\0');
		}
		return rmvSecParen(paren, ++stringLength);
	}
	
	public static int check(char paren[] , int stringLength)
	{
		if(stringLength == - 1)
			return 0;
		if (paren[stringLength] == '(')
			return 1;
		return check(paren, -- stringLength);
	}
}