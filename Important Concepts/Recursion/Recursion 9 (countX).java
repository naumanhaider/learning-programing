import java.util.Scanner;
class recur{
	public static void main ( String args[]){
		Scanner q = new Scanner( System.in );
		String a = q.nextLine();
		int b = countX(a);
		System.out.println(b);
	}
	
	public static int countX(String a){
		if ( a.length()-1 == -1 ) return 0;
		if ( a.charAt(a.length()-1) == 'x' ) return 1 + countX(a.substring(0, a.length()-1));
		return 0 + countX(a.substring(0, a.length()-1));
	}
}