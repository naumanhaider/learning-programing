#include<stdio.h>
#include<iostream>
#include<cstdlib>

using namespace std;

struct node
{
    int data;
    node *next;
};

bool isEmpty(node *head);
void inSert(node *&head, node *&last, int num);
void reMove(node *head, node *last);
void showList(node *show);
char menu();

bool isEmpty(node *head)
{
    if(head == NULL)
        return true;
    return false;
}

void inSert(node *&head, node *&last, int num)
{
    node *temp = new node;
    temp->data = num;
    temp->next = NULL;
    if(isEmpty(head))
    {
        head = temp;
        last = temp;
    }
    else
    {
        last->next = temp;
        last = temp;
    }
}

void reMove(node *head, node *last)
{
    if(isEmpty(head))
        cout << "The List is Already Emtpy\n";
    else if(head == last)
    {
        delete head;
        head = NULL;
        last = NULL;
    }
    else
    {
        node *temp = head;
        head = head->next;
        delete temp;
    }
}

void showList(node *show)
{
    if(isEmpty(show))
        cout << "The List is Empty\n";
    else
    {
        cout << "The List Contains:\n";
        while(show != NULL)
        {
            cout << show->data <<" ";
            show = show->next;
        }
        cout << endl;
    }
}

char menu()
{
    char choice;

    cout << "Menu\n1. Add an item\n";
    cout << "2. Remove an item\n";
    cout << "3. Show the List\n";
    cout << "4. Exit\n";

    cin >> choice;

    return choice;
}

int main()
{
    node *head = NULL;
    node *last = NULL;
    char choice = '5';
    int number;
    while (choice != '4')
    {
        choice = menu();

        if(choice == '1')
        {
            cout << "Please Enter the Number: ";
            cin >> number;
            inSert(head,last,number);
        }
        if(choice == '2')
            reMove(head,last);
        if(choice == '3')
            showList(head);
    }
    return 0;
}
