import java.util.Scanner;
class recur{
	public static void main ( String args[]){
		Scanner q = new Scanner( System.in );
		String a = q.nextLine();
		int b = countHi(a);
		System.out.println(b);
	}
	
	public static int countHi(String a){
		if ( a.length()-1 == -1 ) return 0;
		if ( a.charAt(a.length() - 1) == 'i' ) {
			if ( a.charAt(a.length() - 2) == 'h' )
				return 1 + countHi(a.substring(0, a.length()-1));
		}
		return 0 + countHi(a.substring(0, a.length()-1));
	}
}