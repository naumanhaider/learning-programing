import java.util.Scanner;
class recur{
	public static void main (String args[]){
		Scanner q = new Scanner ( System.in );
		System.out.println("Please enter a Number");
		int a = q.nextInt();
		System.out.println("The number you have entered is: " + a + " and the sum of its digits is");
		a = sumDigits( a );
		System.out.println(a);
	}
	public static int sumDigits(int a){
		if (a == 0) return 0;
		int b = a % 10;
		a = a / 10;
		return b + sumDigits(a);
	}
}
//http://www.tutorialscollection.com/java-foreach-how-to-use-for-each-loop-in-java/