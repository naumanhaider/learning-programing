import java.util.*;
class Palindrome
{
	public static void main (String args[])
	{
		Scanner scanner = new Scanner(System.in);
		String userInput;
		userInput = scanner.nextLine();
		int length, isPalindrome;
		length = userInput.length();
		System.out.println(length);
		isPalindrome = palindrome( userInput , 0 , length - 1 );
		if( isPalindrome == 1)
		{
			System.out.println("This String is Palindrome");
		}
		if( isPalindrome == 0)
		{
			System.out.println("it is not a palindrome");
		}
	}
	public static int palindrome(String userInput, int startIndex, int endIndex)
	{
		int ret = 0;
		if(startIndex == endIndex || startIndex == (endIndex - 1)) return 1;
		
		if ( userInput.charAt(startIndex) == userInput.charAt(endIndex) )
		{
			ret = palindrome ( userInput , startIndex + 1 , endIndex - 1 );

		}
		return ret;
	}
}