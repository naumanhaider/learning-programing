#include<iostream>
#include<cstring>
#include<cstdlib>

char* rmvFirstParen (char *paren, int stringLength);//Prototype Declaration
char* rmvSecParen (char* paren, int stringLength);
int checkSec (char* paren, int stringLength);

int check = 0;

using namespace std;

int main()
{
	char usrInput[30];
	char *final;
	cin>>usrInput; //take User Input
	int stringLength = strlen(usrInput);
	final = rmvFirstParen(usrInput, --stringLength); //Send the array to the first function
	int temp = checkSec(final, --stringLength); //Sends the string for a final check.
	if(check == 1)
        cout<<"Misaligned bracket detected"<<endl;
	cout<<final; // Prints out the final string that has no brackets or has misalligned brackets.
	system("pause");
	return 0;
}

char* rmvFirstParen (char *paren, int stringLength) // Its a recursive Function Of Finding The Bracket and Removing it.
{
	if (stringLength == 0)
        return paren;
	if (paren[stringLength] == ')' ) //Here it has found the first Bracket
	{
        int pointer = stringLength - 1;// Stores the index of the First bracket into the Pointer.
        stringLength = strlen(paren);
        int temp = checkSec(paren, --stringLength); // Send it to a function to check for the Second Bracket.
        if (check == 1)
        {
            do
            {
                pointer++;
                paren[pointer] = paren[pointer + 1]; //If the second Bracket is found it removes the first one.
            }while (paren[pointer] != '\0');
        }
        if (check == 0)
        {
            cout<<"Misaligned bracket detected"<<endl;
            return paren;
        }
        paren = rmvSecParen(paren, --stringLength); // and Send the array to another function that will be removing its Second alligned Bracket.
        return rmvFirstParen(paren, --stringLength);
	}
	return rmvFirstParen(paren, --stringLength);
}

char* rmvSecParen (char* paren, int stringLength)// Function For the Second Bracket.
{
    if (stringLength == 0)
        return paren;
	if (paren[stringLength] == '(' )
	{
        int pointer = stringLength - 1;
        do
        {
            pointer++;
            paren[pointer] = paren[pointer + 1];
        }while (paren[pointer] != '\0');
    }
    return rmvSecParen(paren, --stringLength);
}

int checkSec (char* paren, int stringLength) //Function to check the Second Bracket. Its return type should have been void
{	if (paren[stringLength] == '(' )//but i couldnt find any other way of ending the function at the if or else statements
    {
        check = 1;
        return 0; // because it has a recursive line at the end and will just go there again.
    }
    if (stringLength == 0)
    {
        check = 0;
        return 0;
    }
    checkSec(paren, --stringLength);
}
