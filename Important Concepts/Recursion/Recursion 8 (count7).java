import java.util.Scanner;
class recur{
	public static void main (String arg[]){
		Scanner q = new Scanner ( System.in );
		int a = q.nextInt();
		a = count7(a);
		System.out.println(a);
	}
	public static int count7 (int a){
		if ( a > 0 && a < 7 && a > 7 && a < 10) return 0;
		if ( a == 7) return 1;
		int b = a % 10;
		a = a / 10;
		if ( b == 7) return 1 + count7( a );
		return count7 ( a );
	}
}