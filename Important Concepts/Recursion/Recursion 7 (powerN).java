import java.util.Scanner;
class recur{
	public static void main (String arg[]){
		Scanner q = new Scanner ( System.in );
		int a , b , c;
		System.out.println("Please Enter a number:");
		a = q.nextInt();
		System.out.println("Please Enter its power");
		b = q.nextInt();
		a = powerN( a , b );
		System.out.println("The answer is: " + a );
	}
	public static int powerN (int a , int b){
		if ( b == 0) return 1;
		return  a * powerN(a , b-1);
	}
}